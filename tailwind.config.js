module.exports = {
  mode: 'jit',
  content: ['./src/pages/**/*.{jsx,tsx}', './src/libraries/**/*.{jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: 'var(--primary-base)',
        neutral: 'var(--neutral-01)',
        neutral02: 'var(--neutral-02)',
        neutral03: 'var(--neutral-03)',
        neutral04: 'var(--neutral-04)',
        neutral05: 'var(--neutral-05)',
        neutral06: 'var(--neutral-06)',
        neutral07: 'var(--neutral-07)',
        neutral08: 'var(--neutral-08)',
        neutral11: 'var(--neutral-11)',
        secondaryRed: {
          "900": 'var(--secondaryRed900)',
          "1000": 'var(--secondaryRed)'
        },
        secondaryYellow: {
          "900": 'var(--secondaryYellow900)',
          "1000": 'var(--secondaryYellow)'
        },
        secondaryBlue: {
          "900": 'var(--secondaryBlue900)',
          "1000": 'var(--secondaryBlue)'
        },
        secondaryGreen: {
          "900": "var(--secondaryGreen900)",
          "1000": 'var(--secondaryGreen)'
        },
        gs2: 'var(--gs2)',
        bgMain: 'var(--bg-main)'
      },
      boxShadow: {
        sh01: '0px 8px 16px rgba(0, 0, 0, 0.15)',
      },
    },
    fontFamily: {
      GoogleSans: ['GoogleSans', 'sans-serif']
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio')
  ]
};
