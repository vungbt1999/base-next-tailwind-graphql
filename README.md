# Start
1. Min Nodejs v12
2. npm install or yarn
3. npm run dev or yarn dev

# Script more
1. npm run format or yarn format ( format source code )
2. npm run build or yarn build ( build project )
3. npm run lint or yarn lint
4. npm run dev or yarn dev

# Technology
1. Next 12
2. TailwindCss 3
3. Headlessui
4. Scss
5. Redux toolkit

# Source base
```bash
public
 - fonts
 - images
 - locales
src
 - constants
 - hooks
    - alert
    - redux
    - rxjs
 - libraries
    - components
    - form-base
    - header-seo
    - layouts
    - modals
 - middleware
 - pages
 - styles
 - utils
```
# State management
1. Redux toolkit used management state of app ( RootState )
2. All state saved in folder ( src/hooks/redux )
