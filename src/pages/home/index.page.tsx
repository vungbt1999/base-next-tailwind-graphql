import MainLayout from 'libraries/layouts/main-layout';
import { withTranslations } from 'middleware/withSSTranslations';
import React from 'react';

export default function HomePage() {
  return <div>HomePage</div>;
}

HomePage.Layout = MainLayout;

export const getServerSideProps = withTranslations();
