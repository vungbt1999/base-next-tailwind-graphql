import useInitHook from 'hooks/useInitHook';
import type { AppProps } from 'next/app';
import { Fragment } from 'react';
import { Provider } from 'react-redux';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'styles/index.scss';
import store from 'utils/redux-store';
import { appWithTranslation } from 'next-i18next';
import AlertBase from 'libraries/components/alert-base';
import ToastBase from 'libraries/components/toast-base';

function Root(props: any) {
  useInitHook();

  return (
    <Fragment>
      {props.children}
      <ToastBase />
      <AlertBase />
    </Fragment>
  );
}
function MyApp({ Component, pageProps }: AppProps) {
  const Layout = (Component as any).Layout || (({ children }: any) => <>{children}</>);

  return (
    <Provider store={store}>
      <Root>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Root>
    </Provider>
  );
}

export default appWithTranslation(MyApp);
