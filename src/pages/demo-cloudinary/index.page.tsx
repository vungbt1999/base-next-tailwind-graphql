/* eslint-disable @next/next/no-img-element */
import MainLayout from 'libraries/layouts/main-layout';
import { withTranslations } from 'middleware/withSSTranslations';
import { useTranslation } from 'next-i18next';
import DemoCloudinaryUtils from 'handles/demo-cloudinary/demo-cloudinary.utils';
import ModalBase from 'libraries/components/modal-base';
import ButtonForm from 'libraries/form-base/buttons/button-form';
import { accepts } from 'constants/common';
import SEOMeta from 'libraries/header-seo/SEOMeta';

export default function DemoPage() {
  const { t } = useTranslation();
  const {
    loadingUpload,
    fileShows,
    showModalChooseFile,
    inputFileRef,
    postFile,
    onOpenModalChooseFile,
    onCloseModalChooseFile,
    onChangeFile,
    onSubmitPost,
    onDeleteFile
  } = DemoCloudinaryUtils();

  return (
    <div className="main-content">
      <SEOMeta title="Demo Cloudinary" />
      <h1 className="my-4">Test Upload File</h1>
      <ButtonForm
        type="button"
        label="Show Modal Upload"
        onClick={onOpenModalChooseFile}
        className="mb-5"
      />

      {postFile.length > 0 && (
        <ButtonForm
          type="button"
          label="Delete File"
          onClick={() => onDeleteFile(postFile.map((item) => item.publicId))}
          className="mb-5 ml-5"
        />
      )}

      {postFile.length <= 0 && <p>Image Empty, Click Button By Upload 😁</p>}
      <div className="flex items-center justify-between my-5 flex-wrap">
        {postFile.map((item, index) => {
          return (
            <div key={index} className="w-32 h-32 max-h-32 rounded border-4 border-white">
              <img
                className="w-full h-full rounded object-cover"
                src={item.url}
                alt="image_upload"
              />
            </div>
          );
        })}
      </div>

      <ModalBase
        visible={showModalChooseFile}
        onCloseModal={onCloseModalChooseFile}
        onClickButtonLeft={onSubmitPost}
        labelButtonLeft="Post Image"
      >
        {loadingUpload && <div className="mt-5">Loading...</div>}
        <input
          accept={accepts.image}
          type="file"
          onChange={(value) => onChangeFile(value?.target?.files)}
          ref={inputFileRef}
          className="mt-5"
        />
        {fileShows && fileShows.length > 0 && (
          <div className="flex items-center justify-between my-5 flex-wrap">
            {fileShows.map((item, index) => {
              return (
                <div key={index} className="w-1/3 h-32 max-h-32 rounded border-4 border-white">
                  <img
                    className="w-full h-full rounded object-cover"
                    src={item.url}
                    alt="image_upload"
                  />
                </div>
              );
            })}
          </div>
        )}
      </ModalBase>
    </div>
  );
}

DemoPage.Layout = MainLayout;

export const getServerSideProps = withTranslations();
