import React, { useRef, useEffect } from 'react';

function useOutsideAlerter(ref: any, cb: any) {
  useEffect(() => {
    if (!document) return;
    function handleClickOutside(event: any) {
      if (ref.current && !ref.current.contains(event.target)) {
        cb();
      }
    }

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);
}

interface Props {
  children: React.ReactNode | React.ReactNodeArray;
  clickOutside: () => void;
}

export default function OutsideAlerter({ children, clickOutside }: Props) {
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, clickOutside);

  return <div ref={wrapperRef}>{children}</div>;
}
