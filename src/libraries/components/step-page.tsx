import clsx from 'clsx';
import React from 'react';
import { IStepPage } from 'types/common.types';

interface Props {
  stepsActive: number[];
  listStep: IStepPage[];
  onChangeTab?: (value: IStepPage) => void;
}

export default function StepPage({ stepsActive, listStep, onChangeTab }: Props) {
  // change tab
  const onChange = (value: IStepPage) => {
    if (onChangeTab) return onChangeTab(value);
  };

  return (
    <div className="step_page">
      {listStep.map((item) => {
        return (
          <div
            className={clsx('step_page_item', {
              'step_page_item-active': stepsActive.includes(item.step)
            })}
            key={item.step}
            onClick={() => onChange(item)}
          >
            <p className="step_page_item-step-number">
              {item.step} <span className="step_page_item-title">{item.title}</span>
            </p>
            <div className="step_page_item-line"></div>
          </div>
        );
      })}
    </div>
  );
}
