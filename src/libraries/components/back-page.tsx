import clsx from 'clsx';
import { useRouter } from 'next/router';
import { FC } from 'react';

interface Props {
  title: string;
  className?: string;
  onBack?: () => void;
}

export const BackPage: FC<Props> = ({ title, className = '', onBack }) => {
  const router = useRouter();

  // on click icon back
  const onClickBack = () => {
    if (onBack) return onBack();
    router.back();
  };

  return (
    <div className={clsx('back-page-common', { [className]: true })}>
      <div onClick={onClickBack} className="back-page-common-icon-view">
        <img
          className="back-page-common-icon"
          src="/images/main/icon-arrow-back.svg"
          alt="icon-arrow-back"
        />
      </div>
      <p className="back-page-common-title">{title}</p>
    </div>
  );
};
