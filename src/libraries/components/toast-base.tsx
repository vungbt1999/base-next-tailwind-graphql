import { ToastSetting } from 'constants/common';
import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import React, { useEffect } from 'react';
import { TypeToast } from 'types/common.types';

export default function ToastBase() {
  useEffect(() => {
    const duration = ToastSetting.duration || 3000;
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === SubjectType.SHOW_TOAST) {
        const { content, type_toast } = data as { content: string; type_toast: TypeToast };
        const mainToast = document.getElementById('toast_id');
        const delay = (ToastSetting.duration / 1000).toFixed(2);
        if (mainToast) {
          const toast = document.createElement('div');

          // Auto remove toast
          const autoRemoveId = setTimeout(() => {
            mainToast.removeChild(toast);
          }, duration + 1000);

          toast.classList.add('toast_notification', `toast_notification-${type_toast}`);
          toast.style.animation = `slideInLeft ease 0.5s, fadeOut linear 1s ${delay}s forwards`;
          toast.innerHTML = `
            <img class='toast_notification-icon' src=${ToastSetting[type_toast].icon} alt="icon-notification" />
            <div class='toast_notification-content'>
              <p class='toast_notification-content-mess'>${content}</p>
            </div>
            <img class='toast_notification-icon-close' src=${ToastSetting[type_toast].icon_close} alt="icon-notification" />
          `;
          mainToast.appendChild(toast);

          // Click remove toast
          toast.addEventListener('click', (e) => {
            if ((e.target as any).className.includes('toast_notification-icon-close')) {
              toast.remove();
              clearTimeout(autoRemoveId);
            }
          });
        }
      }
    });
    return () => subscribe.unsubscribe();
  }, []);

  return <div id="toast_id"></div>;
}
