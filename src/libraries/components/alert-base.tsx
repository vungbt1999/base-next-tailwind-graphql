import { AlertSetting } from 'constants/common';
import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import React, { useEffect } from 'react';
import { TypeToast } from 'types/common.types';

export default function AlertBase() {
  useEffect(() => {
    const duration = AlertSetting.duration || 3000;
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === SubjectType.SHOW_ALERT) {
        const { content, type_toast } = data as { content: string; type_toast: TypeToast };
        const mainAlert = document.getElementById('alert_id');
        const delay = (AlertSetting.duration / 1000).toFixed(2);
        if (mainAlert) {
          const alert = document.createElement('div');

          // Auto remove alert
          const autoRemoveId = setTimeout(() => {
            mainAlert.removeChild(alert);
          }, duration + 1000);

          alert.classList.add('alert_notification', `alert_notification-${type_toast}`);
          alert.style.animation = `slideInLeft ease 0.5s, fadeOut linear 1s ${delay}s forwards`;
          alert.innerHTML = `
            <img class='alert_notification-icon' src=${AlertSetting[type_toast].icon} alt="icon-notification" />
            <div class='alert_notification-content'>
              <p class='alert_notification-content-mess'>${content}</p>
            </div>
            <img class='alert_notification-icon-close' src=${AlertSetting[type_toast].icon_close} alt="icon-notification" />
          `;
          mainAlert.appendChild(alert);

          // Click remove alert
          alert.addEventListener('click', (e) => {
            if ((e.target as any).className.includes('alert_notification-icon-close')) {
              alert.remove();
              clearTimeout(autoRemoveId);
            }
          });
        }
      }
    });
    return () => subscribe.unsubscribe();
  }, []);

  return <div id="alert_id"></div>;
}
