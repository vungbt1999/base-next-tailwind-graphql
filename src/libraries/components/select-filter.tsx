import clsx from 'clsx';
import { useTranslation } from 'next-i18next';
import React, { useEffect, useState } from 'react';
import OutsideAlerter from './outside-alerter';

export enum ValueSelectFilter {
  Newest = 'Newest',
  Oldest = 'Oldest',
  Hight_To_Low = 'Hight_To_Low',
  Low_To_Hight = 'Low_To_Hight'
}

export interface OptionSelectFilterItem {
  label: string;
  value: string;
}

interface Props {
  options?: OptionSelectFilterItem[];
  onChange: (value: OptionSelectFilterItem) => void;
  active?: OptionSelectFilterItem;
}

export default function SelectFilter({ options, onChange, active }: Props) {
  const { t } = useTranslation();
  const [optionsRender, setOptionRender] = useState<OptionSelectFilterItem[]>([]);
  const [showDropDown, setShowDropDown] = useState<boolean>(false);
  const [activeFilter, setActiveFilter] = useState<OptionSelectFilterItem>();

  const listSort: OptionSelectFilterItem[] = [
    {
      label: t('select_filter.newest'),
      value: ValueSelectFilter.Newest
    },
    {
      label: t('select_filter.oldest'),
      value: ValueSelectFilter.Oldest
    },
    {
      label: t('select_filter.hight_to_low'),
      value: ValueSelectFilter.Hight_To_Low
    },
    {
      label: t('select_filter.low_to_hight'),
      value: ValueSelectFilter.Low_To_Hight
    }
  ];

  useEffect(() => {
    if (options) return setOptionRender(options);
    setOptionRender(listSort);
  }, [options]);

  useEffect(() => {
    if (active) return setActiveFilter(active);
    if (options) return setActiveFilter(options[0]);
    setActiveFilter(listSort[0]);
  }, [active, options]);

  const onOutSideLocal = () => {
    setShowDropDown(false);
  };

  const onShowListLocal = () => {
    setShowDropDown(!showDropDown);
  };

  const onChangeFilter = (item: OptionSelectFilterItem) => {
    onChange(item);
    setShowDropDown(false);
  };

  return (
    <div className="select-filter-wrapper">
      <OutsideAlerter clickOutside={onOutSideLocal}>
        <div className="select-filter-item-active" onClick={onShowListLocal}>
          <p className="name">{activeFilter?.label}</p>
          <img
            className="local-dropdown_icon"
            src="/images/main/arrow-down.svg"
            alt="french-logo"
          />
        </div>
        <div
          className={clsx('list-locale', {
            'show-list-locale': showDropDown
          })}
        >
          {optionsRender.map((item, index) => {
            return (
              <div key={index} className="item-local" onClick={() => onChangeFilter(item)}>
                <p
                  className={clsx('locale-name', {
                    'local-name-active': activeFilter?.value === item.value
                  })}
                >
                  {item.label}
                </p>
                <img
                  className={clsx('icon-locale-active', {
                    'show-active-icon': activeFilter?.value === item.value
                  })}
                  src="/images/locales/icon-active.svg"
                  alt="icon-locale-active"
                />
              </div>
            );
          })}
        </div>
      </OutsideAlerter>
    </div>
  );
}
