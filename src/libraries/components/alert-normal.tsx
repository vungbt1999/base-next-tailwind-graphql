import clsx from 'clsx';
import { AlertSetting } from 'constants/common';
import React from 'react';
import { TypeToast } from 'types/common.types';

interface Props {
  suffixIconUrl?: string;
  content?: string;
  type_toast?: TypeToast;

  onClose?: () => void;
}
export default function AlertNormal({
  suffixIconUrl = AlertSetting[TypeToast.INFO].icon,
  content,
  type_toast = TypeToast.INFO,

  onClose
}: Props) {
  return (
    <div
      className={clsx('alert_notification_normal', `alert_notification_normal-${type_toast}`, {
        'alert_notification_normal-hidden': !content || content?.length <= 0
      })}
    >
      <img className="alert_notification_normal-icon" src={suffixIconUrl} alt="icon-notification" />
      <div className="alert_notification_normal-content">
        <p className="alert_notification_normal-content-mess">{content}</p>
      </div>
      <img
        onClick={onClose}
        className="alert_notification_normal-icon-close"
        src={AlertSetting[type_toast].icon_close}
        alt="icon-notification"
      />
    </div>
  );
}
