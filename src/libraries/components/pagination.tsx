import clsx from 'clsx';
import IconChevrons from 'libraries/icon-components/chevrons';
import React from 'react';
import ReactPaginate from 'react-paginate';

interface Props {
  pageCount: number;
  onChangePage: (offSet: number) => void;
  className?: string;
}

export default function Pagination({ pageCount, onChangePage, className }: Props) {
  const renderItemPageFL = (isNext: boolean = true) => {
    return (
      <div className={clsx('pageItem flex text-sm py-4 items-center', isNext && 'rotate-180')}>
        <IconChevrons />
        <IconChevrons />
      </div>
    );
  };
  const renderItemPage = (page: number) => {
    return (
      <div className={clsx('pageItem selected-page flex text-sm sm:py-4 items-center')}>{page}</div>
    );
  };

  return (
    <div className="pagination-container">
      <ReactPaginate
        breakLabel="..."
        breakClassName="break-item !pt-1"
        marginPagesDisplayed={1}
        nextLabel={renderItemPageFL()}
        pageRangeDisplayed={2}
        pageCount={pageCount}
        // pageClassName="pageItem"
        previousLabel={renderItemPageFL(false)}
        pageLabelBuilder={(page: number) => renderItemPage(page)}
        renderOnZeroPageCount={() => null}
        className={clsx('flex paging', className)}
        onClick={(event: any) => {
          if (event.nextSelectedPage === undefined) return;
          onChangePage(event.nextSelectedPage);
        }}
      />
    </div>
  );
}
