/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx';
import { useTranslation } from 'next-i18next';
import React, { useEffect } from 'react';
import HamburgerIcon from './components/hamburger-icon';
import LocaleSelect from './components/locale-select';
import MenuMobile, { IMenuMobile } from './components/menu-mobile';

interface Props {
  onClickMenuMobile: (key: string) => void;
  activeMenuMobile?: string;
  listMenuMobile: IMenuMobile[];
}
export default function HeaderLayout({
  onClickMenuMobile,
  activeMenuMobile,
  listMenuMobile
}: Props) {
  const { t } = useTranslation();

  return (
    <div className="header-layout">
      <div className="main-content content-header">
        <div className="left-content">
          <img className="logo-page" src="/images/logo.svg" alt="logo-page" />
        </div>
        <div className="right-content">
          <p className="support-text">{t('header.support')}</p>
          <LocaleSelect />
          <p className="login-button">{t('login')}</p>
          <p className="register-button">{t('register')}</p>
          <HamburgerIcon />
          <input hidden type="checkbox" id="toggle_menu_icon" className="checkbox4" />
          <label htmlFor="toggle_menu_icon" className="nav-mobile-overlay"></label>
          <MenuMobile
            onClickMenuMobile={onClickMenuMobile}
            activeMenuMobile={activeMenuMobile}
            listMenuMobile={listMenuMobile}
          />
        </div>
      </div>
    </div>
  );
}
