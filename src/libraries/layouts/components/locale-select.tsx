/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx';
import useLocale, { LangItem } from 'hooks/redux/locale/useLocale';
import OutsideAlerter from 'libraries/components/outside-alerter';
import React, { useEffect, useState } from 'react';

export default function LocaleSelect() {
  const { langs, setLocale, locale } = useLocale();

  const [activeLocale, setActiveLocale] = useState<LangItem>({
    value: process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'en',
    label: 'French',
    image_url: '/images/locales/french.svg'
  });
  const [showDropdown, setShowDropdown] = useState<boolean>(false);

  useEffect(() => {
    if (locale) {
      const item = langs.find((x) => x.value === locale);
      if (item) {
        setActiveLocale(item);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [locale]);

  // show list local
  const onShowListLocal = () => {
    setShowDropdown(!showDropdown);
  };

  // click outside local
  const onOutSideLocal = () => {
    setShowDropdown(false);
  };

  const onChangeLocale = (item: LangItem) => {
    setShowDropdown(false);
    setLocale(item.value);
  };

  return (
    <div className="locale-select-wrapper">
      <div className="locale-select-item-active" onClick={onShowListLocal}>
        <img className="local-icon" src={activeLocale.image_url} alt="french-logo" />
        <img className="local-dropdown_icon" src="/images/main/arrow-down.svg" alt="french-logo" />
      </div>
      <OutsideAlerter clickOutside={onOutSideLocal}>
        <div
          className={clsx('list-locale', {
            'show-list-locale': showDropdown
          })}
        >
          {langs.map((item, index) => {
            return (
              <div key={index} className="item-local" onClick={() => onChangeLocale(item)}>
                <img className="local-icon" src={item.image_url} alt="french-logo" />
                <p className="locale-name">{item.label}</p>
                <img
                  className={clsx('icon-locale-active', {
                    'show-active-icon': activeLocale.value === item.value
                  })}
                  src="/images/locales/icon-active.svg"
                  alt="icon-locale-active"
                />
              </div>
            );
          })}
        </div>
      </OutsideAlerter>
    </div>
  );
}
