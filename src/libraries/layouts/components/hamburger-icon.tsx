import React from 'react';

export default function HamburgerIcon() {
  return (
    <div className="hamburger_icon">
      <label htmlFor="toggle_menu_icon">
        <div className="hamburger hamburger4">
          <span className="bar bar1"></span>
          <span className="bar bar2"></span>
          <span className="bar bar3"></span>
          <span className="bar bar4"></span>
          <span className="bar bar5"></span>
        </div>
      </label>
    </div>
  );
}
