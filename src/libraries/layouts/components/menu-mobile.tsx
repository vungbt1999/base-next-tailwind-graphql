import clsx from 'clsx';
import React from 'react';

export enum MenuMobileKey {
  SETTING = 'SETTING',
  ORDER = 'ORDER',
  CHANGE_PASSWORD = 'CHANGE_PASSWORD',
  LOGOUT = 'LOGOUT'
}

export interface IMenuMobile {
  key: string;
  icon: string;
  icon_active: string;
  label: string;
}

interface Props {
  onClickMenuMobile: (key: string) => void;
  activeMenuMobile?: string;
  listMenuMobile: IMenuMobile[];
}

export default function MenuMobile({ onClickMenuMobile, activeMenuMobile, listMenuMobile }: Props) {
  return (
    <div className="menu-mobile">
      <div className="menu-mobile-header">
        <div className="menu-mobile_info-user">
          <img
            className="menu-mobile_avatar"
            src="https://joeschmoe.io/api/v1/random"
            alt="avatar-demo"
          />
          <p className="menu-mobile_name-user">Jerome Bell</p>
        </div>

        <label htmlFor="toggle_menu_icon">
          <img className="menu-mobile_icon-close" src="/images/main/icon-x.svg" alt="icon-close" />
        </label>
      </div>

      {/** Menu Content */}
      <div className="menu-mobile_content">
        {listMenuMobile.map((item) => {
          return (
            <label
              htmlFor="toggle_menu_icon"
              key={item.key}
              className={clsx('menu-mobile_item', {
                ['menu-mobile_item-active']: activeMenuMobile === item.key
              })}
              onClick={() => onClickMenuMobile(item.key)}
            >
              <img
                className="menu-mobile-item_icon"
                src={activeMenuMobile === item.key ? item.icon_active : item.icon}
                alt={`icon-${item.key}`}
              />
              <p className="menu-mobile-item-label">{item.label}</p>
            </label>
          );
        })}
      </div>
    </div>
  );
}
