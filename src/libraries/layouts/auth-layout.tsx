import SEOMeta, { SEOMetaProps } from 'libraries/header-seo/SEOMeta';

interface Props extends SEOMetaProps {
  children: any;
}

export const AuthLayout = (props: Props) => {
  const { children } = props;
  return (
    <div className="sign-up-container relative">
      <div className="hidden sm:block flex-1">
        <img
          className="w-full object-cover h-[100vh]"
          src="/images/background/login-background.svg"
          alt="logo-xx"
        />
      </div>
      <div className="right-container w-1/2">{children}</div>
    </div>
  );
};
