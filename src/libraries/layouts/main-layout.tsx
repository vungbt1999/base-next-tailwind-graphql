import React, { Fragment, useState } from 'react';
import SEOMeta, { SEOMetaProps } from 'libraries/header-seo/SEOMeta';
import HeaderLayout from './header-layout';
import FooterLayout from './footer-layout';
import { IMenuMobile, MenuMobileKey } from './components/menu-mobile';
import { useTranslation } from 'next-i18next';

interface Props extends SEOMetaProps {
  children: any;
}

export default function MainLayout(props: Props) {
  const { children } = props;
  const [activeMenuMobile, setActiveMenuMobile] = useState<string>();
  const { t } = useTranslation();
  const listMenuMobile: IMenuMobile[] = [
    {
      key: MenuMobileKey.SETTING,
      icon: '/images/header/menu-mobile/icon-user-setting-unactive.svg',
      icon_active: '/images/header/menu-mobile/icon-user-setting.svg',
      label: t('menu.account_setting')
    },
    {
      key: MenuMobileKey.ORDER,
      icon: '/images/header/menu-mobile/icon-user-order-unactive.svg',
      icon_active: '/images/header/menu-mobile/icon-user-order.svg',
      label: t('menu.order_manager')
    },
    {
      key: MenuMobileKey.CHANGE_PASSWORD,
      icon: '/images/header/menu-mobile/icon-change-password-unactive.svg',
      icon_active: '/images/header/menu-mobile/icon-change-password.svg',
      label: t('menu.change_password')
    },
    {
      key: MenuMobileKey.LOGOUT,
      icon: '/images/header/menu-mobile/icon-logout-unactive.svg',
      icon_active: '/images/header/menu-mobile/icon-logout.svg',
      label: t('menu.log_out')
    }
  ];
  const onClickMenuMobile = (key: string) => {
    switch (key) {
      case MenuMobileKey.SETTING: {
        console.log('<====setting====>');
        setActiveMenuMobile(key);
        break;
      }
      case MenuMobileKey.ORDER: {
        console.log('<====order====>');
        setActiveMenuMobile(key);
        break;
      }
      case MenuMobileKey.CHANGE_PASSWORD: {
        console.log('<====change password====>');
        setActiveMenuMobile(key);
        break;
      }
      case MenuMobileKey.LOGOUT: {
        console.log('<====logout====>');
        setActiveMenuMobile(key);
        break;
      }
    }
  };

  return (
    <div className="main-layout">
      <SEOMeta {...props} />
      <HeaderLayout
        onClickMenuMobile={onClickMenuMobile}
        activeMenuMobile={activeMenuMobile}
        listMenuMobile={listMenuMobile}
      />
      <div>{children}</div>
      <FooterLayout />
    </div>
  );
}
