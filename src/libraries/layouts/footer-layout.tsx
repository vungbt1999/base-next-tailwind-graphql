import clsx from 'clsx';
import { useTranslation } from 'next-i18next';
import React from 'react';

export default function FooterLayout() {
  const { t } = useTranslation();
  const listSocial = [
    {
      key: 1,
      icon_url: '/images/footer/facebook.svg'
    },
    {
      key: 2,
      icon_url: '/images/footer/twitter.svg'
    },
    {
      key: 3,
      icon_url: '/images/footer/telegram.svg'
    }
  ];
  return (
    <div className={clsx('footer-layout')}>
      <div className="main-content content-footer">
        <div className="left-content">
          <img className="logo-page" src="/images/logo.svg" alt="logo-page" />
        </div>
        <div className="right-content">
          <p className="contact-title">{t('footer.contact')}</p>
          <div className="list-social-contact">
            <ul className="social-view">
              {listSocial.map((item) => {
                return (
                  <li className="social-contact-item" key={item.key}>
                    <img className="social-contact-icon" src={item.icon_url} alt="icon-social" />
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
      <p className="copyright">{t('footer.copyright')}</p>
    </div>
  );
}
