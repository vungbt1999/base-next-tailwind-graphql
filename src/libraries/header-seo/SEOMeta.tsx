import Head from 'next/head';
import React from 'react';

export type SEOMetaProps = {
  title?: string;
  description?: string;
  keywords?: string;
  robots?: string;
};

export default function SEOMeta(props: SEOMetaProps) {
  const { title = process.env.NEXT_PUBLIC_WEB_NAME, description, robots, keywords } = props;
  return (
    <Head>
      <title>{title}</title>
      <meta name="google" content="notranslate" />
      <meta name="description" content={description || title} />
      <meta name="keywords" content={keywords} />
      <meta name="robots" content={robots || 'index, follow'} />
    </Head>
  );
}
