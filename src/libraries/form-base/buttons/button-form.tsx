import clsx from 'clsx';
import { useTranslation } from 'next-i18next';
import React from 'react';

interface Props {
  label?: string;
  className?: string;
  type?: 'button' | 'submit' | 'reset';
  height?: 36 | 40 | 44 | 48 | 56;
  buttonType?: 'primary' | 'outline';
  onClick?: () => void;
  disable?: boolean;
}

export default function ButtonForm({
  label,
  type = 'submit',
  className = '',
  height = 36,
  buttonType = 'primary',
  onClick,
  disable = false
}: Props) {
  const { t } = useTranslation();
  return (
    <button
      disabled={disable}
      type={type}
      className={clsx(
        'form_base-button-submit ',
        {
          'h-9': height === 36,
          'h-10': height === 40,
          'h-11': height === 44,
          'h-12': height === 48,
          'h-14': height === 56,
          'form_base-button-submit-primary': buttonType === 'primary',
          'form_base-button-submit-outline': buttonType === 'outline',
          [className]: true
        },
        disable && 'cursor-no-drop'
      )}
      onClick={onClick}
    >
      {label || t('book_button')}
    </button>
  );
}
