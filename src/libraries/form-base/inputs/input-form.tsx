import clsx from 'clsx';
import { ErrorMessage } from 'formik';
import React from 'react';

interface Props {
  field: any;
  form: any;

  placeholder?: string;
  showMess?: boolean;
  label?: string;
  classNameError?: string;
  suffixIcon?: string;
  prefixIcon?: string;
  height?: 36 | 40;
  className?: any;
}
export default function InputForm({
  field,
  form,

  placeholder,
  showMess = true,
  label,
  suffixIcon,
  prefixIcon,
  classNameError = '',
  height = 36,
  className
}: Props) {
  const { name, value, onChange, onBlur } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  return (
    <div
      className={clsx('form_base_input-view', {
        [className]: true
      })}
    >
      <label
        htmlFor={name}
        className={clsx('form_base-label', {
          'form_base-label-hidden': !label
        })}
      >
        {label}
      </label>

      {/** Input Main */}
      <label
        htmlFor={name}
        className={clsx('form_base-input-main', {
          'form_base-input-main-error': showError && showMess,
          'h-9 rounded': height === 36,
          'h-10 rounded-lg': height === 40
        })}
      >
        {prefixIcon && (
          <img
            className={clsx('form_base-input-prefix-icon', {
              'w-5 h-5': height === 40,
              'w-4 h-4': height === 36
            })}
            src={prefixIcon}
            alt="prefix-icon-input"
          />
        )}
        <input
          id={name}
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          className="form_base-input"
          placeholder={placeholder}
        />
        {suffixIcon && (
          <img
            className={clsx('form_base-input-suffix-icon', {
              'w-5 h-5': height === 40,
              'w-4 h-4': height === 36
            })}
            src={suffixIcon}
            alt="suffix-icon-input"
          />
        )}
      </label>

      {/** Mess Error */}
      <div
        className={clsx('form_base-mess-error', {
          'form_base-hidden-mess-error': !showError,
          [classNameError]: true
        })}
      >
        {showError && showMess && <ErrorMessage name={name} />}
      </div>
    </div>
  );
}
