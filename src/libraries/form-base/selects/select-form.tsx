import { Listbox } from '@headlessui/react';
import { CheckIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import { ErrorMessage } from 'formik';
import React, { Fragment, useEffect, useState } from 'react';

export interface SelectOption {
  label: string;
  value: string;
}

interface Props {
  field: any;
  form: any;

  placeholder?: string;
  showMess?: boolean;
  label?: string;
  classNameError?: string;
  suffixIcon?: string;
  options: SelectOption[];
  allowClear?: boolean;
  allowClearIcon?: string;
  height?: 36 | 40;
}
export default function SelectForm({
  field,
  form,

  placeholder,
  showMess = true,
  label,
  suffixIcon = '/images/main/icon-arrow-select.svg',
  options,
  allowClear = false,
  allowClearIcon = '/images/main/icon-x.svg',
  classNameError = '',
  height = 36
}: Props) {
  const { name, value, onChange, onBlur } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];
  const [selected, setSelected] = useState<SelectOption | undefined>();

  const selectedOption = options.find((x) => x.value === value);

  const onChangeSelectItem = (values: SelectOption) => {
    setSelected(values);
    const changeEvent = {
      target: {
        name: name,
        value: values.value
      }
    };
    onChange(changeEvent);
  };

  // clear value select
  const onClearValueSelect = () => {
    setSelected(undefined);
    const changeEvent = {
      target: {
        name: name,
        value: undefined
      }
    };
    onChange(changeEvent);
  };

  return (
    <div className="form_base_select-view">
      <label
        htmlFor={name}
        className={clsx('form_base-label', {
          'form_base-label-hidden': !label
        })}
      >
        {label}
      </label>

      <Listbox value={selected} onChange={onChangeSelectItem}>
        <div className="form_base-select-box">
          <img
            className={clsx('form_base-allow-clear-icon', {
              'form_base-allow-clear-icon-show': allowClear
            })}
            src={allowClearIcon}
            alt="icon-allow-clear-icon"
            onClick={onClearValueSelect}
          />
          <Listbox.Button
            className={clsx('form_base-select-active-view', {
              'form_base-select-active-view-error': showError && showMess,
              'h-9 rounded': height === 36,
              'h-10 rounded-lg': height === 40
            })}
          >
            <p
              className={clsx('form_base-select-active-label', {
                'form_base-select-active-have-value': selectedOption?.label
              })}
            >
              {selectedOption?.label ? selectedOption?.label : placeholder || ''}
            </p>
            <img
              className={clsx('form_base-input-suffix-icon', {
                'w-5 h-5': height === 40,
                'w-4 h-4': height === 36
              })}
              src={suffixIcon}
              alt="icon-suffix-select"
            />
            <input
              hidden
              id={name}
              name={name}
              value={value}
              onChange={onChange}
              onBlur={onBlur}
              className="form_base-input border"
              placeholder={placeholder}
            />
          </Listbox.Button>

          <Listbox.Options className="form_base-list-box-select">
            {options.map((item, index) => (
              <Listbox.Option
                key={index}
                className={() => {
                  return clsx('form_base-list-box-option', {
                    'form_base-list-box-option-active': selectedOption?.value === item.value
                  });
                }}
                value={item}
              >
                {() => {
                  return (
                    <Fragment>
                      <span className="icon-checked">
                        <CheckIcon className="w-5 h-5" aria-hidden="true" />
                      </span>
                      <div className="form_base-icon-counter"></div>
                      <span>{item.label}</span>
                    </Fragment>
                  );
                }}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </div>
      </Listbox>

      {/** Mess Error */}
      <div
        className={clsx('form_base-mess-error', {
          'form_base-hidden-mess-error': !showError,
          [classNameError]: true
        })}
      >
        {showError && showMess && <ErrorMessage name={name} />}
      </div>
    </div>
  );
}
