import clsx from 'clsx';
import { H_MM_AA } from 'constants/common';
import { ErrorMessage } from 'formik';
import { useTranslation } from 'next-i18next';
import React from 'react';
import DatePicker from 'react-datepicker';

interface Props {
  field: any;
  form: any;

  placeholder?: string;
  showMess?: boolean;
  label?: string;
  classNameError?: string;
  suffixIcon?: string;
  timeFormat?: string;
  timeCaption?: string;
  height?: 36 | 40;
}
export default function TimePickerForm({
  field,
  form,

  placeholder,
  showMess = true,
  label,
  suffixIcon = '/images/main/icon-clock.svg',
  timeCaption,
  timeFormat = H_MM_AA,
  classNameError = '',
  height = 36
}: Props) {
  const { t } = useTranslation();
  const { name, value, onChange, onBlur } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  const onChangeDatePicker = (value: Date | null) => {
    const changeEvent = {
      target: {
        name: name,
        value: value
      }
    };
    onChange(changeEvent);
  };

  return (
    <div className="form_base_input-view">
      <label
        htmlFor={name}
        className={clsx('form_base-label', {
          'form_base-label-hidden': !label
        })}
      >
        {label}
      </label>

      {/** DatePicker Main */}
      <label
        htmlFor={name}
        className={clsx('form_base-input-main', 'form_base-date-picker-main', {
          'form_base-input-main-error': showError && showMess,
          'h-9 rounded': height === 36,
          'h-10 rounded-lg': height === 40
        })}
      >
        <DatePicker
          id={name}
          name={name}
          value={value}
          selected={value}
          onChange={onChangeDatePicker}
          onBlur={onBlur}
          className="form_base-date-picker"
          placeholderText={placeholder}
          onChangeRaw={(e) => e.preventDefault()}
          autoComplete="off"
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={15}
          timeCaption={timeCaption || t('time_cation')}
          dateFormat={timeFormat}
        />
        {suffixIcon && (
          <img
            className={clsx('form_base-input-suffix-icon', {
              'w-5 h-5': height === 40,
              'w-4 h-4': height === 36
            })}
            src={suffixIcon}
            alt="suffix-icon-input"
          />
        )}
      </label>

      {/** Mess Error */}
      <div
        className={clsx('form_base-mess-error', {
          'form_base-hidden-mess-error': !showError,
          [classNameError]: true
        })}
      >
        {showError && showMess && <ErrorMessage name={name} />}
      </div>
    </div>
  );
}
