import {} from 'react';
import cx from 'classnames';

interface Props {
  onClick?: () => void;
  classLabel?: string;
  label?: any;
  classBox?: string;
}

export const CheckBox = (props: Props) => {
  const { classLabel, classBox, onClick, label } = props;

  return (
    <div className="flex ">
      <input
        id="checkbox-all-search"
        type="checkbox"
        className={cx(
          'w-4 h-4 text-secondaryBlue-900 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600',
          classBox
        )}
        onClick={onClick}
      />
      <label
        htmlFor="checkbox-all-search"
        className={cx(' text-sm font-GoogleSans font-medium', `${label ? 'ml-3' : ''}`, classLabel)}
      >
        {label}
      </label>
    </div>
  );
};
