import React from 'react';

interface Props {
  label?: string;
  id: string;
  name: string;
  value: string;
}

export default function RadioForm(props: Props) {
  const { id, name, label, value } = props;
  return (
    <div className="flex items-center mb-4">
      <input
        id={id}
        type="radio"
        name={name}
        value={value}
        className="w-4 h-4 border-gray-300 focus:ring-2 focus:ring-blue-300 dark:focus:ring-blue-600 dark:focus:bg-blue-600 dark:bg-gray-700 dark:border-gray-600"
        aria-labelledby={id}
        aria-describedby={id}
      />
      <label
        htmlFor={id}
        className="block ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
      >
        {label}
      </label>
    </div>
  );
}
