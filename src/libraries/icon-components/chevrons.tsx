export default function IconChevrons(props: any) {
  return (
    <svg
      width="5"
      height="8"
      viewBox="0 0 5 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M4.4714 0.195262C4.73175 0.455612 4.73175 0.877722 4.4714 1.13807L1.60948 4L4.4714 6.86193C4.73175 7.12228 4.73175 7.54439 4.4714 7.80474C4.21106 8.06509 3.78894 8.06509 3.5286 7.80474L0.195262 4.4714C-0.0650874 4.21106 -0.0650874 3.78894 0.195262 3.5286L3.5286 0.195262C3.78894 -0.0650874 4.21106 -0.0650874 4.4714 0.195262Z"
        fill="#23262F"
      />
    </svg>
  );
}
