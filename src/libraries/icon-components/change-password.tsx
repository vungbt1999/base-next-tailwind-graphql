function IconChangePassword(props: any) {
  return (
    <svg
      width="22"
      height="22"
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M15.5972 9.67578H6.3916V14.4449H15.5972V9.67578Z"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
      <path
        d="M14.1392 9.51545V9.67595H7.85026C7.83887 9.61863 7.83887 9.57278 7.83887 9.51545C7.83887 7.76142 9.25161 6.33984 10.9947 6.33984C12.7265 6.33984 14.1392 7.76142 14.1392 9.51545Z"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
      <path
        d="M19.7214 16.1872C17.9669 19.1564 14.754 21.1397 11.0741 21.1397C5.50286 21.1397 0.991211 16.5999 0.991211 10.9938C0.991211 10.6958 1.0026 10.3862 1.03678 10.0996"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
      <path
        d="M20.8942 13.2408C21.0651 12.5186 21.1449 11.7619 21.1449 10.9938C21.1449 5.39924 16.6332 0.859375 11.0734 0.859375C6.90357 0.859375 3.33754 3.39299 1.78809 7.01571"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
      <path
        d="M20.3248 19.5693L20.0742 15.6943L16.3828 16.5771"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
      <path
        d="M0.842773 3.39258L1.458 7.41655L5.37721 6.06376"
        stroke="currentColor"
        stroke-width="1.5"
        stroke-miterlimit="10"
      />
    </svg>
  );
}

export default IconChangePassword;
