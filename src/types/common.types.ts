import { User } from 'graphql/generated/graphql';

export interface AuthLocal {
  tokenInfo: TokenInfo;
  user: User;
}

export interface TokenInfo {
  accessToken: string;
  refreshToken: string;
  expireTime: number;
}

export interface RefreshTokenReq {
  refreshToken: string;
}

export enum TypeToast {
  SUCCESS = 'success',
  INFO = 'info',
  WARNING = 'warning',
  ERROR = 'error'
}

export interface IStepPage {
  title: string;
  step: number;
}

export interface IFileCloudinary {
  bytes: number;
  created_at: string;
  folder: string;
  format: string;
  public_id: string;
  resource_type: string;
  secure_url: string;
  signature: string;
  url: string;
}

export interface IFile {
  url: string;
  publicId: string;
  metadata: {
    bytes: number;
    created_at: string;
    folder: string;
    format: string;
    public_id: string;
    resource_type: string;
    secure_url: string;
    signature: string;
  };
}
