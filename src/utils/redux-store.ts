import { configureStore, ConfigureStoreOptions, getDefaultMiddleware } from '@reduxjs/toolkit';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';
import baseReducer from 'hooks/redux/reducer';

const rootReducer = {
  ...baseReducer
};

const isClient = typeof window !== 'undefined';

const options: ConfigureStoreOptions = {
  reducer: rootReducer
};

options.middleware = getDefaultMiddleware();
if (isClient) {
  options.middleware = options.middleware.concat([
    createStateSyncMiddleware({
      whitelist: ['auth/changeAuth', 'locale/changeLocale']
    })
  ]);
}

const store = configureStore(options);

if (isClient) {
  initMessageListener(store);
}

export type RootState = ReturnType<typeof store.getState>;
export default store;
