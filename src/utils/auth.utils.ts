import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import { AuthLocal, RefreshTokenReq } from 'types/common.types';
import localStorageUtils, { KeyStorage } from './local-storage.utils';

export const setAuth = (auth: AuthLocal | null) => {
  localStorageUtils.setObject(KeyStorage.AUTH, auth);
  subject.next({
    type: SubjectType.SET_AUTH,
    data: auth
  });
};

export const refreshToken = async (variables: RefreshTokenReq) => {
  // TO DO
};

export const getAuth = async () => {
  try {
    const auth: AuthLocal = localStorageUtils.getObject(KeyStorage.AUTH, null);
    if (auth) {
      const tokenInfo = auth.tokenInfo;
      const expireTime = tokenInfo.expireTime;
      const timeRefreshToken = (Date.now() + 60 * 1000) / 1000;
      if (timeRefreshToken < expireTime) {
        return { ...auth, tokenInfo: tokenInfo };
      } else {
        // refreshToken 1 minute before token expiration
        // const variables: RefreshTokenReq = {
        //   refreshToken: tokenInfo?.refreshToken
        // };
        // const result = await refreshToken(variables);
        // if (result) {
        //   setAuth({ ...auth, tokenInfo: result });
        //   return { ...auth, tokenInfo: result };
        // }
      }
      return { ...auth, tokenInfo: tokenInfo };
    }
  } catch (error) {
    setAuth(null);
    return null;
  }
};
