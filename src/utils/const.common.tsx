export enum PageUrl {
  LOGIN = '/auth/login',
  CHANGE_PASSWORD = '/auth/change-password',
  RESET_PASSWORD = '/auth/reset-password',
  SIGN_UP = '/auth/sign-up',
  FORGET_PASSWORD = '/auth/forget-password'
}
