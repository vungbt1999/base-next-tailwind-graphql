import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import localStorageUtils, { KeyStorage } from 'utils/local-storage.utils';
import { getAuth } from 'utils/auth.utils';

export type HeaderConf = {
  authorization?: boolean;
  locale?: string;
  deleteCacheIds?: string[];
  fetchPolicy?: 'no-cache' | 'cache-first' | 'cache-only' | 'network-only' | 'standby';
} & Record<string, unknown>;

export const getAccessToken = async () => {
  const auth = await getAuth();
  if (auth) {
    const tokenInfo = auth.tokenInfo;
    return tokenInfo.accessToken;
  }
  return null;
};

export const getHeader = async (headerConf: HeaderConf = {}) => {
  const { authorization, locale, ...reset } = headerConf;
  const headers = { ...reset };
  if (authorization) {
    headers['Authorization'] = await getAccessToken();
  }
  if (locale) {
    headers['Accept-Language'] = locale;
  } else {
    if (process.browser) {
      headers['Accept-Language'] = localStorageUtils.getObject(KeyStorage.LOCALE)?.key;
    }
  }
  return headers;
};

const httpLink = createHttpLink({
  uri: process.env.NEXT_PUBLIC_API_DOMAIN || 'http://localhost:3000/graphql'
});

const authMiddleware = setContext(async (_, { headers = {} }) => {
  let headerConfig = {};
  // get token
  let token = null;
  const { isAuth } = headers;
  if (isAuth) {
    token = await getAccessToken();
    if (!token) {
      localStorageUtils.clear();
    }
  }
  headerConfig = {
    ...headers,
    authorization: isAuth ? `Bearer ${token}` : null
  };
  return { headers: headerConfig };
});

// set up apolloClient
const clientConfig = new ApolloClient({
  cache: new InMemoryCache(),
  link: authMiddleware.concat(httpLink)
});

export default clientConfig;
