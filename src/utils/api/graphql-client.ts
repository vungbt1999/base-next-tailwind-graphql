// import env from '@/env';
import {
  ApolloClient,
  ApolloLink,
  DocumentNode,
  HttpLink,
  InMemoryCache,
  OperationVariables,
  split
} from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { GraphQLClient } from 'graphql-request';
import { TokenInfo } from 'types/common.types';
import { getAuth } from 'utils/auth.utils';
import { getSdk, SdkFunctionWrapper } from '../../graphql/generated/graphql';

// const endpoint = env.apiEndPoint;

const endpoint = process.env.NEXT_PUBLIC_API_DOMAIN || 'http://localhost:3000/graphql';

const httpLink = new HttpLink({
  uri: endpoint
});

export const getAccessToken = async () => {
  // const auth = getAuthToken();
  const auth = await getAuth();
  if (auth) {
    const tokenInfo = auth.tokenInfo as TokenInfo;
    return tokenInfo.accessToken;
  }
  return null;
};

const link = split(({ query }) => {
  const definition = getMainDefinition(query);
  return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
}, httpLink);

export const getClient = async (auth = true, signal?: AbortSignal) => {
  const headers: any = {};
  if (auth) {
    headers.authorization = auth ? `Bearer ${await getAccessToken()}` : undefined;
  }

  const graphQLClient = new GraphQLClient(endpoint, {
    headers
  });

  return graphQLClient;
};

export const clientSub = new ApolloClient({
  link: ApolloLink.from([link]),
  cache: new InMemoryCache()
});

export const subScription = (query: DocumentNode, variables?: OperationVariables) => {
  return clientSub.subscribe({
    query,
    variables
    // fetchPolicy: 'no-cache'
  });
};
const clientResWrapper: SdkFunctionWrapper = async <T>(action: () => Promise<T>): Promise<any> => {
  try {
    const result = await action();
    return result;
  } catch (error: any) {
    // : { errorType: string, message: string, statusCode: string }
    const resErr = error.response.errors[0];
    if (resErr) {
      const statusCode = resErr.extensions?.exception?.response?.statusCode;
      if (statusCode === 401) {
        await getAuth();
        const result = await action();
        return result;
      }
    }
    throw resErr;
  }
};

export const getSDK = async (auth = true, signal?: AbortSignal) => {
  const client = await getClient(auth, signal);
  return getSdk(client, clientResWrapper);
};

export default {
  getClient,
  getSDK
};
