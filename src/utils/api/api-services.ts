import { ApolloCache } from '@apollo/client';
import to from 'await-to-js';
import { DocumentNode } from 'graphql';
import { HeaderConf } from 'utils/api/api-client.utils';
import { getAuth } from 'utils/auth.utils';
import clientConfig from './api-client.utils';

const deleteCache = (cache: ApolloCache<any>, cacheIds?: string[]) => {
  cacheIds?.forEach((cacheId) => {
    const [__typename, id] = cacheId.split(':');
    const normalizedId = cache.identify({ id, __typename });
    cache.evict({ id: normalizedId });
    cache.gc();
  });
};

const graphqlClient = {
  async query(requestTo: DocumentNode, variables?: any, headerCof?: HeaderConf) {
    const header = headerCof || { authorization: true };
    const res = await to(
      clientConfig.query({
        query: requestTo,
        variables: { ...variables },
        context: {
          headers: {
            isAuth: header.authorization
          }
        },
        fetchPolicy: headerCof?.fetchPolicy || 'no-cache'
      })
    );

    // check error
    let error: any;
    [error] = res;
    if (error) {
      const errorData = error?.graphQLErrors[0];
      const statusCode = errorData?.statusCode;
      if (statusCode == 401) {
        getAuth();
      }
    }
    return res;
  },

  async mutation(requestTo: DocumentNode, variables?: any, headerCof?: HeaderConf) {
    const header = headerCof || { authorization: true };
    const res = await to(
      clientConfig.mutate({
        mutation: requestTo,
        variables: { ...variables },
        context: {
          headers: {
            isAuth: header.authorization
          }
        },
        update(cache: ApolloCache<any>) {
          deleteCache(cache, headerCof?.deleteCacheIds);
        }
      })
    );

    // check error
    let error: any;
    [error] = res;
    if (error) {
      const errorData = error?.graphQLErrors[0];
      const statusCode = errorData?.statusCode;
      if (statusCode == 401) {
        getAuth();
      }
    }
    return res;
  },

  async resetStore() {
    await clientConfig.resetStore();
  }
};

export default graphqlClient;
