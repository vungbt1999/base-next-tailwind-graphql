import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import { TypeToast } from 'types/common.types';

export const showAlertSuccess = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_ALERT,
    data: {
      content: content || '',
      type_toast: TypeToast.SUCCESS
    }
  });
};

export const showAlertInfo = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_ALERT,
    data: {
      content: content || '',
      type_toast: TypeToast.INFO
    }
  });
};

export const showAlertError = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_ALERT,
    data: {
      content: content || '',
      type_toast: TypeToast.ERROR
    }
  });
};

export const showAlertWarning = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_ALERT,
    data: {
      content: content || '',
      type_toast: TypeToast.WARNING
    }
  });
};

export const showToastSuccess = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_TOAST,
    data: {
      content: content || '',
      type_toast: TypeToast.SUCCESS
    }
  });
};

export const showToastError = (content: string): void => {
  subject.next({
    type: SubjectType.SHOW_TOAST,
    data: {
      content: content || '',
      type_toast: TypeToast.ERROR
    }
  });
};
