import { useRef, useState } from 'react';
import {
  apiDeleteFile,
  apiGetSignUrl,
  apiMoveFileToNewFolder,
  apiUploadFile
} from './services/index';
import { IFile } from '../../types/common.types';

interface Utils {
  loadingUpload: boolean;
  showModalChooseFile: boolean;
  inputFileRef?: React.LegacyRef<HTMLInputElement>;
  fileShows: { publicId: string; url: string }[];
  postFile: IFile[];
  onOpenModalChooseFile: () => void;
  onCloseModalChooseFile: () => void;
  onChangeFile: (files: FileList | null) => void;
  onSubmitPost: () => void;
  onDeleteFile: (publicIds: string[]) => void;
}

export default function DemoCloudinaryUtils(): Utils {
  const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
  const [fileShows, setFileShows] = useState<{ publicId: string; url: string }[]>([]);

  const inputFileRef = useRef<any>();
  const [showModalChooseFile, setShowModalChooseFile] = useState<boolean>(false);
  const [postFile, setPostFile] = useState<IFile[]>([]);

  const onOpenModalChooseFile = () => {
    setShowModalChooseFile(true);
  };

  const onCloseModalChooseFile = () => {
    setShowModalChooseFile(false);
    setFileShows([]);
  };

  const onChangeFile = async (files: FileList | null) => {
    if (!files || files.length <= 0) return;
    const file = files[0];
    onClickGetUrlUpload(file);
  };

  const onClickGetUrlUpload = async (file: File) => {
    if (loadingUpload) return;
    setLoadingUpload(true);
    const resSignUrl = await apiGetSignUrl({ fileName: file.name });
    if (!resSignUrl || Object.values(resSignUrl).length <= 0) return;
    const formData = new FormData();
    formData.append('file', file);
    const res = await apiUploadFile(resSignUrl.get_sign_url.url, formData);
    setLoadingUpload(false);
    if (inputFileRef) {
      const inputFile = inputFileRef.current;
      inputFile.value = null;
    }

    const newpublicIds = [...fileShows];
    newpublicIds.push({ publicId: res.public_id, url: res.url });
    setFileShows(newpublicIds);
  };

  // post file
  const onSubmitPost = async () => {
    const getpublicId = fileShows.map((item) => item.publicId);
    const res = await apiMoveFileToNewFolder({ oldPublicIds: getpublicId });
    console.log('res post:', res);
    setPostFile(res.move_file);
    setFileShows([]);
    setShowModalChooseFile(false);
  };

  const onDeleteFile = async (publicIds: string[]) => {
    const res = await apiDeleteFile({ publicIds });
    console.log('res post:', res);
  };

  return {
    loadingUpload,
    showModalChooseFile,
    inputFileRef,
    fileShows,
    postFile,
    onOpenModalChooseFile,
    onCloseModalChooseFile,
    onChangeFile,
    onSubmitPost,
    onDeleteFile
  };
}
