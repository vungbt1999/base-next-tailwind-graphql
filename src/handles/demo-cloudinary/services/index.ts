import { getSDK } from 'utils/api/graphql-client';
import {
  SignUrlReq,
  MoveFileCloudinaryReq,
  DeleteFileCloudinaryReq
} from 'graphql/generated/graphql';
import axios from 'axios';
import { IFileCloudinary } from 'types/common.types';

export const apiGetSignUrl = async (args: SignUrlReq) => {
  const sdk = await getSDK(true);
  return sdk.getSignUrl({ args });
};

export const apiUploadFile = async (url: string, data: FormData) => {
  const res = await axios.post(url, data);
  return res.data as IFileCloudinary;
};

export const apiMoveFileToNewFolder = async (args: MoveFileCloudinaryReq) => {
  const sdk = await getSDK(true);
  return sdk.moveFile({ args });
};

export const apiDeleteFile = async (args: DeleteFileCloudinaryReq) => {
  const sdk = await getSDK(true);
  return sdk.deleteFile({ args });
};
