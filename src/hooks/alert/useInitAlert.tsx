import { useEffect } from 'react';
import { showAlertError } from 'utils/common.utils';
import { subject, SubjectType } from '../rxjs/useInitSubject';

export default function useInitAlert() {
  useEffect(() => {
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === SubjectType.SET_MESSAGE) {
        showAlertError(data);
      }
    });
    return () => subscribe.unsubscribe();
  }, []);
}
