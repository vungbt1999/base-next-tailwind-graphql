import { useEffect } from 'react';
import { Subject } from 'rxjs';

export type EventSubscribe = { type: string; data: any };
export enum SubjectType {
  SET_MESSAGE = 'SET_MESSAGE',
  SET_AUTH = 'SET_AUTH',
  SHOW_ALERT = 'SHOW_ALERT',
  SHOW_TOAST = 'SHOW_TOAST'
}
export const subject = new Subject();

export default function useInitSubject() {
  useEffect(() => {
    const subscribe = subject.subscribe();
    return () => subscribe.unsubscribe();
  }, []);
}
