import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageUtils, { KeyStorage } from 'utils/local-storage.utils';
import _ from 'lodash';
import { fallBackImage } from 'constants/common';

interface Locale {}

export const locales: any = {
  fr: { locale: 'fr', image_url: '/images/locales/french.svg', name: 'French' },
  en: { locale: 'en', image_url: '/images/locales/english.svg', name: 'English' }
};

const defaultLocaleName = process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'fr';

const initialState: Locale =
  localStorageUtils.getObject(KeyStorage.LOCALE, null) || locales[defaultLocaleName].locale;

const locale = createSlice({
  name: KeyStorage.LOCALE,
  initialState: initialState,
  reducers: {
    changeLocale: (state: Locale, action: PayloadAction<Locale>) => {
      state = action.payload;
      return state;
    }
  }
});

const { reducer, actions } = locale;
export const { changeLocale } = actions;
export default reducer;
