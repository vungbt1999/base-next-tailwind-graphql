import localStorageHelper, { KeyStorage } from 'utils/local-storage.utils';
import moment from 'moment';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import useLocale from './useLocale';
import { registerLocale } from 'react-datepicker';
import fr from 'date-fns/locale/fr';
import en from 'date-fns/locale/en-AU';

function useInitLocale() {
  const router = useRouter();
  const { locale } = useLocale();
  useEffect(() => {
    if (locale !== router.locale) {
      moment.locale(locale);
      localStorageHelper.setObject(KeyStorage.LOCALE, locale);
      router.replace(router.asPath, router.asPath, { locale: locale });
      if (locale === 'fr') {
        registerLocale('fr', fr);
      } else {
        registerLocale('en', en);
      }
    }
  }, [locale]);
}

export default useInitLocale;
