import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'utils/redux-store';
import { changeLocale, locales } from './reducer';

export type LocaleName = keyof typeof locales;
export interface LangItem {
  value: LocaleName;
  label: string;
  image_url: string;
}
const langs: LangItem[] = [];

for (const [key, value] of Object.entries(locales)) {
  langs.push({
    value: key as LocaleName,
    label: (value as any).name,
    image_url: (value as any).image_url
  });
}

function useLocale() {
  const locale = useSelector((state: RootState) => state.locale);
  const dispatch = useDispatch();
  const setLocale = (newLocale: LocaleName) => {
    const actionChangeLocale = changeLocale(locales[newLocale].locale);
    dispatch(actionChangeLocale);
  };
  return { locale, langs, setLocale };
}

export default useLocale;
