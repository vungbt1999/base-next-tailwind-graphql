import locale from './locale/reducer';
import auth from './auth/reducer';

const baseReducer = {
  locale,
  auth
};

export default baseReducer;
