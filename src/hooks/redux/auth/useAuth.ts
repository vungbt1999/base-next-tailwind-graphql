import { useSelector } from 'react-redux';
import { getAuth, setAuth } from 'utils/auth.utils';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { changeAuth } from './reducer';
import graphqlClient from 'utils/api/api-services';
import { RootState } from 'utils/redux-store';
import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import localStorageUtils from 'utils/local-storage.utils';

export default function useAuth() {
  const dispatch = useDispatch();
  const auth = useSelector((state: RootState) => state?.auth);

  useEffect(() => {
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === SubjectType.SET_AUTH) {
        if (data !== auth) {
          dispatch(changeAuth(data));
        }
      }
    });
    return () => subscribe.unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subject]);

  // remove all store
  const clearStore = async () => {
    setAuth(null);
    await graphqlClient.resetStore();
    await localStorageUtils.clear();
  };

  return { auth, setAuth, getAuth, clearStore };
}
