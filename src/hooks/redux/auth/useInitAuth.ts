import { subject, SubjectType } from 'hooks/rxjs/useInitSubject';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { changeAuth } from './reducer';
import useAuth from './useAuth';

export default function useInitAuthClient() {
  const { auth, getAuth } = useAuth();
  const dispatch = useDispatch();

  useEffect(() => {
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === SubjectType.SET_AUTH) {
        if (data !== auth) {
          dispatch(changeAuth(data));
        }
      }
    });
    return () => subscribe.unsubscribe();
  }, []);

  useEffect(() => {
    getAuth();
  }, []);
}
