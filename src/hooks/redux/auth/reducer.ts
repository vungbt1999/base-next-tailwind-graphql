import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AuthLocal } from 'types/common.types';
import localStorageUtils, { KeyStorage } from 'utils/local-storage.utils';

const initialState: AuthLocal | null = localStorageUtils.getObject(KeyStorage.AUTH, null);

const auth = createSlice({
  name: KeyStorage.AUTH,
  initialState: initialState,
  reducers: {
    changeAuth: (state: AuthLocal | null, action: PayloadAction<AuthLocal | null>) => {
      localStorageUtils.setObject(KeyStorage.AUTH, action.payload);
      state = action.payload;
      return state;
    }
  }
});

const { reducer, actions } = auth;
export const { changeAuth } = actions;
export default reducer;
