import useInitAlert from './alert/useInitAlert';
import useInitSubject from './rxjs/useInitSubject';
import useInitLocale from './redux/locale/useInitLocale';
import useInitAuth from './redux/auth/useInitAuth';

export default function useInitHook() {
  useInitSubject();
  useInitAlert();
  useInitLocale();
  useInitAuth();
}
