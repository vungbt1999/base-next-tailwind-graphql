const path = require('path');

module.exports = {
    i18n: {
        defaultLocale: process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'en',
        locales: ['en', 'fr'],
        defaultNS: 'common',
        localePath: path.resolve('./public/locales'),
        reloadOnPrerender: process.env.NODE_ENV === 'development',
        localeDetection: false,
    }
};
