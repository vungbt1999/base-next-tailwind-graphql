const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n,
  pageExtensions: ['page.ts', 'page.tsx'],
  future: {
    strictPostcssConfiguration: true
  },
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/home'
      }
    ];
  },
  experimental: {
    esmExternals: false
  },
}

module.exports = nextConfig
